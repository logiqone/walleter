package workers_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	//"develop/protobuf/wallet"
	//"github.com/jackc/pgx/pgtype"
	//"time"

	protobufWallet "develop/protobuf/wallet"
	protobufMain "develop/protobuf/main"

	"develop/wallet/models/db"
	//"develop/main"
	"develop/main"
	"math/rand"
	"strconv"
	"develop/queue"
)

func testRefund(client protobufWallet.WalleterClient) {
	Describe("ProgressRefund()", func() {
		Context("correct request without and with optional fields and correct hold amount", func() {
			It("should create bill and put it in queue", func() {
				dbWallet := createWallet(testWalletUserId)
				defer deleteWallet(dbWallet.Id)

				// without optional
				dbRequest := createRequest(dbWallet.Id)

				billAmount := rand.Float64()

				dbTransactionHold := createTransaction(dbWallet.Id)
				dbTransactionHold.Target = protobufWallet.TransactionTargetHold
				dbTransactionHold.Reference = protobufWallet.TransactionReferenceRequest
				dbTransactionHold.ReferenceId = strconv.FormatInt(dbRequest.Id, 10)
				dbTransactionHold.After = billAmount + 1
				Ω(dbTransactionHold.Save()).Should(BeNil())


				protoBill := &protobufWallet.Bill{
					Request: 		&protobufWallet.Request{Id: dbRequest.Id},
					Amount: 	billAmount,
					Gateway:	protobufWallet.Gateway(randIntMapKey(protobufWallet.Gateway_name)),
					WithdrawId: &protobufMain.Int64{Value: randInt64()},
				}

				subscriber, err := queue.NewSubscriber(queue.Queue{
					Name:     "testing.bill.refund",
					Exchange: queue.EventInExchange,
					Key:      queue.EventKey("bill.refund"),
				})
				Ω(err).Should(BeNil())
				events := subscriber.Subscribe(main.Ctx())

				_, err = client.ProgressRefund(main.Ctx(), protoBill)
				Ω(err).Should(BeNil())

				dbBillFromDb := new(db.Bill)
				Ω(dbBillFromDb.FindLastByWalletAndType(protobufWallet.BillTypeRefund, dbWallet.Id)).Should(BeNil())

				Ω(dbBillFromDb.RequestId).To(Equal(dbRequest.Id))
				Ω(dbBillFromDb.Amount).To(Equal(billAmount))
				Ω(dbBillFromDb.WithdrawId.Int).Should(Equal(protoBill.WithdrawId.Value))
				Ω(dbBillFromDb.Type).To(Equal(protobufWallet.BillTypeRefund))
				Ω(dbBillFromDb.Status).To(Equal(protobufWallet.BillStatusProgressed))

				event := <- events
				protoBillFromQueue := new(protobufWallet.Bill)
				Ω(protoBillFromQueue.Unmarshal(event.Delivery.Body)).Should(BeNil())
				event.Delivery.Ack(false)

				Ω(protoBillFromQueue.Request.Id).Should(Equal(dbRequest.Id))
				Ω(protoBillFromQueue.Amount).Should(Equal(billAmount))
				Ω(protoBillFromQueue.Gateway).Should(Equal(protoBill.Gateway))
				Ω(protoBillFromQueue.WithdrawId.Value).Should(Equal(protoBill.WithdrawId.Value))
				Ω(protoBillFromQueue.Type).Should(Equal(protobufWallet.BillTypeRefund))
				Ω(protoBillFromQueue.Status).Should(Equal(protobufWallet.BillStatusProgressed))

				// with optional
				dbRequest2 := createRequest(dbWallet.Id)

				dbTransactionHold2 := createTransaction(dbWallet.Id)
				dbTransactionHold2.Target = protobufWallet.TransactionTargetHold
				dbTransactionHold2.Reference = protobufWallet.TransactionReferenceRequest
				dbTransactionHold2.ReferenceId = strconv.FormatInt(dbRequest2.Id, 10)
				dbTransactionHold2.After = billAmount + 1
				Ω(dbTransactionHold2.Save()).Should(BeNil())

				protoBill2 := &protobufWallet.Bill{
					Request: 			&protobufWallet.Request{Id: dbRequest2.Id},
					Amount: 		billAmount,
					Gateway:		protobufWallet.Gateway(randIntMapKey(protobufWallet.Gateway_name)),
					WithdrawId:		&protobufMain.Int64{Value: randInt64()},

					ReferenceId:	&protobufMain.String{Value: randStringRunes(255)},
					Problem:		protobufWallet.BillProblem(randIntMapKey(protobufWallet.BillProblem_name)),
					Mask:			&protobufMain.String{Value: randStringRunes(255)},
					Country:		protobufMain.Country(randIntMapKey(protobufMain.Country_name)),
					ExchangeRate:   &protobufMain.Double{Value: rand.Float64()},
				}

				_, err = client.ProgressRefund(main.Ctx(), protoBill2)
				Ω(err).Should(BeNil())

				dbBillFromDb2 := new(db.Bill)
				Ω(dbBillFromDb2.FindLastByWalletAndType(protobufWallet.BillTypeRefund, dbWallet.Id)).Should(BeNil())

				Ω(dbBillFromDb2.RequestId).To(Equal(dbRequest2.Id))
				Ω(dbBillFromDb2.Amount).To(Equal(billAmount))
				Ω(dbBillFromDb2.WithdrawId.Int).Should(Equal(protoBill2.WithdrawId.Value))
				Ω(dbBillFromDb2.Type).To(Equal(protobufWallet.BillTypeRefund))
				Ω(dbBillFromDb2.Status).To(Equal(protobufWallet.BillStatusProgressed))

				Ω(dbBillFromDb2.ReferenceId.String).To(Equal(protoBill2.ReferenceId.Value))
				Ω(dbBillFromDb2.Problem).To(Equal(protoBill2.Problem))
				Ω(dbBillFromDb2.Mask.String).To(Equal(protoBill2.Mask.Value))
				Ω(dbBillFromDb2.Country).To(Equal(protoBill2.Country))
				Ω(dbBillFromDb2.ExchangeRate.Float).To(Equal(protoBill2.ExchangeRate.Value))

				event2 := <- events
				protoBillFromQueue2 := new(protobufWallet.Bill)
				Ω(protoBillFromQueue2.Unmarshal(event2.Delivery.Body)).Should(BeNil())
				event2.Delivery.Ack(false)

				Ω(protoBillFromQueue2.Request.Id).Should(Equal(dbRequest2.Id))
				Ω(protoBillFromQueue2.Amount).Should(Equal(billAmount))
				Ω(protoBillFromQueue2.Gateway).Should(Equal(protoBill2.Gateway))
				Ω(protoBillFromQueue2.WithdrawId.Value).Should(Equal(protoBill2.WithdrawId.Value))
				Ω(protoBillFromQueue2.Type).Should(Equal(protobufWallet.BillTypeRefund))
				Ω(protoBillFromQueue2.Status).Should(Equal(protobufWallet.BillStatusProgressed))

				Ω(protoBillFromQueue2.ReferenceId.Value).To(Equal(protoBill2.ReferenceId.Value))
				Ω(protoBillFromQueue2.Problem).To(Equal(protoBill2.Problem))
				Ω(protoBillFromQueue2.Mask.Value).To(Equal(protoBill2.Mask.Value))
				Ω(protoBillFromQueue2.Country).To(Equal(protoBill2.Country))
				Ω(protoBillFromQueue2.ExchangeRate.Value).To(Equal(protoBill2.ExchangeRate.Value))

				subscriber.Remove()
			})
		})
		Context("incorrect request", func() {
			It("should return err", func() {
				dbWallet := createWallet(testWalletUserId)
				defer deleteWallet(dbWallet.Id)
				dbRequest := createRequest(dbWallet.Id)

				protoBill := &protobufWallet.Bill{
					Request: 		&protobufWallet.Request{Id: dbRequest.Id},
					Gateway:	protobufWallet.Gateway(randIntMapKey(protobufWallet.Gateway_name)),
					WithdrawId: &protobufMain.Int64{Value: randInt64()},
				}
				_, err := client.ProgressRefund(main.Ctx(), protoBill)
				Ω(err).ShouldNot(BeNil())


				protoBill = &protobufWallet.Bill{
					Amount: 	rand.Float64(),
					Gateway:	protobufWallet.Gateway(randIntMapKey(protobufWallet.Gateway_name)),
					WithdrawId: &protobufMain.Int64{Value: randInt64()},
				}
				_, err = client.ProgressRefund(main.Ctx(), protoBill)
				Ω(err).ShouldNot(BeNil())


				protoBill = &protobufWallet.Bill{
					Request: 		&protobufWallet.Request{Id: dbRequest.Id},
					Amount: 	rand.Float64(),
					WithdrawId: &protobufMain.Int64{Value: randInt64()},
				}
				_, err = client.ProgressRefund(main.Ctx(), protoBill)
				Ω(err).ShouldNot(BeNil())


				protoBill = &protobufWallet.Bill{
					Request: 		&protobufWallet.Request{Id: dbRequest.Id},
					Amount: 	rand.Float64(),
					Gateway:	protobufWallet.Gateway(randIntMapKey(protobufWallet.Gateway_name)),
				}
				_, err = client.ProgressRefund(main.Ctx(), protoBill)
				Ω(err).ShouldNot(BeNil())
			})
		})
		Context("incorrect amount", func() {
			It("should return err", func() {
				dbWallet := createWallet(testWalletUserId)
				defer deleteWallet(dbWallet.Id)
				dbRequest := createRequest(dbWallet.Id)

				billAmount := rand.Float64()

				dbTransactionHold := createTransaction(dbWallet.Id)
				dbTransactionHold.Target = protobufWallet.TransactionTargetHold
				dbTransactionHold.Reference = protobufWallet.TransactionReferenceRequest
				dbTransactionHold.ReferenceId = strconv.FormatInt(dbRequest.Id, 10)
				dbTransactionHold.After = billAmount - 1
				Ω(dbTransactionHold.Save()).Should(BeNil())

				protoBill := &protobufWallet.Bill{
					Amount: 	billAmount,
					Request: 		&protobufWallet.Request{Id: dbRequest.Id},
					Gateway:	protobufWallet.Gateway(randIntMapKey(protobufWallet.Gateway_name)),
					WithdrawId: &protobufMain.Int64{Value: randInt64()},
				}
				_, err := client.ProgressRefund(main.Ctx(), protoBill)
				Ω(err).ShouldNot(BeNil())
			})
		})
	})

	Describe("AcceptRefund()", func() {
		Context("inaccessible refund amount", func() {
			It("should return err", func() {
				dbWallet := createWallet(testWalletUserId)
				defer deleteWallet(dbWallet.Id)
				dbRequest := createRequest(dbWallet.Id)
				dbBill := createBill(dbRequest.Id)
				dbBill.Amount = 100
				Ω(dbBill.Save()).Should(BeNil())

				dbTransactionHold := createTransaction(dbWallet.Id)
				dbTransactionHold.Target = protobufWallet.TransactionTargetHold
				dbTransactionHold.Reference = protobufWallet.TransactionReferenceRequest
				dbTransactionHold.ReferenceId = strconv.FormatInt(dbRequest.Id, 10)
				dbTransactionHold.After = dbBill.Amount - 1
				Ω(dbTransactionHold.Save()).Should(BeNil())

				_, err := client.AcceptRefund(main.Ctx(), &protobufWallet.Bill{
					Id: dbBill.Id,
				})
				Ω(err).ShouldNot(BeNil())
			})
		})
		Context("incorrect Bill id", func() {
			It("should return err", func() {
				dbWallet := createWallet(testWalletUserId)
				defer deleteWallet(dbWallet.Id)

				_, err := client.AcceptRefund(main.Ctx(), &protobufWallet.Bill{
					Id: randInt64(),
				})
				Ω(err).ShouldNot(BeNil())
			})
		})
		Context("correct request last refund", func() {
			It("should refresh bill status to accepted, request status to accepted, create transaction hold with bill amount", func() {
				dbWallet := createWallet(testWalletUserId)
				defer deleteWallet(dbWallet.Id)
				dbRequest := createRequest(dbWallet.Id)
				dbBill := createBill(dbRequest.Id)
				dbBill.Amount = 100
				Ω(dbBill.Save()).Should(BeNil())

				dbTransactionHold := createTransaction(dbWallet.Id)
				dbTransactionHold.Target = protobufWallet.TransactionTargetHold
				dbTransactionHold.Reference = protobufWallet.TransactionReferenceRequest
				dbTransactionHold.ReferenceId = strconv.FormatInt(dbRequest.Id, 10)
				dbTransactionHold.After = dbBill.Amount
				Ω(dbTransactionHold.Save()).Should(BeNil())

				_, err := client.AcceptRefund(main.Ctx(), &protobufWallet.Bill{
					Id: dbBill.Id,
				})
				Ω(err).Should(BeNil())

				dbBillFromDb := new(db.Bill)
				Ω(dbBillFromDb.FindById(dbBill.Id)).Should(BeNil())
				Ω(dbBillFromDb.Status).Should(Equal(protobufWallet.BillStatusAccepted))

				dbRequestFromDb := new(db.Request)
				Ω(dbRequestFromDb.FindById(dbRequest.Id)).Should(BeNil())
				Ω(dbRequestFromDb.Status).Should(Equal(protobufWallet.RequestStatusAccepted))

				dbTransactionLastFromDb := new(db.Transaction)
				Ω(dbTransactionLastFromDb.FindLastByTargetAndReference(protobufWallet.TransactionTargetHold,
					protobufWallet.TransactionReferenceBill, dbBill.Id)).Should(BeNil())

				Ω(dbTransactionLastFromDb.WalletId).Should(Equal(dbWallet.Id))
				Ω(dbTransactionLastFromDb.Type).Should(Equal(protobufWallet.TransactionTypeRefund))
				Ω(dbTransactionLastFromDb.Direction).Should(Equal(protobufWallet.TransactionDirectionDecrease))
				Ω(dbTransactionLastFromDb.Before).Should(Equal(dbTransactionHold.After))
				Ω(dbTransactionLastFromDb.Amount).Should(Equal(dbBill.Amount))
				Ω(dbTransactionLastFromDb.After).Should(Equal(dbTransactionHold.After - dbBill.Amount))
			})
		})
		Context("correct request not last refund", func() {
			It("should refresh bill status to accepted, request status NOT accepted, create transaction hold with bill amount", func() {
				dbWallet := createWallet(testWalletUserId)
				defer deleteWallet(dbWallet.Id)
				dbRequest := createRequest(dbWallet.Id)
				dbBill := createBill(dbRequest.Id)
				dbBill.Amount = 100
				Ω(dbBill.Save()).Should(BeNil())

				dbTransactionHold := createTransaction(dbWallet.Id)
				dbTransactionHold.Target = protobufWallet.TransactionTargetHold
				dbTransactionHold.Reference = protobufWallet.TransactionReferenceRequest
				dbTransactionHold.ReferenceId = strconv.FormatInt(dbRequest.Id, 10)
				dbTransactionHold.After = dbBill.Amount + 1
				Ω(dbTransactionHold.Save()).Should(BeNil())

				_, err := client.AcceptRefund(main.Ctx(), &protobufWallet.Bill{
					Id: dbBill.Id,
				})
				Ω(err).Should(BeNil())

				dbBillFromDb := new(db.Bill)
				Ω(dbBillFromDb.FindById(dbBill.Id)).Should(BeNil())
				Ω(dbBillFromDb.Status).Should(Equal(protobufWallet.BillStatusAccepted))

				dbRequestFromDb := new(db.Request)
				Ω(dbRequestFromDb.FindById(dbRequest.Id)).Should(BeNil())
				Ω(dbRequestFromDb.Status).ShouldNot(Equal(protobufWallet.RequestStatusAccepted))

				dbTransactionLastFromDb := new(db.Transaction)
				Ω(dbTransactionLastFromDb.FindLastByTargetAndReference(protobufWallet.TransactionTargetHold,
					protobufWallet.TransactionReferenceBill, dbBill.Id)).Should(BeNil())

				Ω(dbTransactionLastFromDb.WalletId).Should(Equal(dbWallet.Id))
				Ω(dbTransactionLastFromDb.Type).Should(Equal(protobufWallet.TransactionTypeRefund))
				Ω(dbTransactionLastFromDb.Direction).Should(Equal(protobufWallet.TransactionDirectionDecrease))
				Ω(dbTransactionLastFromDb.Before).Should(Equal(dbTransactionHold.After))
				Ω(dbTransactionLastFromDb.Amount).Should(Equal(dbBill.Amount))
				Ω(dbTransactionLastFromDb.After).Should(Equal(dbTransactionHold.After - dbBill.Amount))
			})
		})
	})

	Describe("DeclineRefund()", func() {
		Context("correct request", func() {
			It("should ...", func() {

			})
		})
	})
}
