package workers_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	protobufWallet "develop/protobuf/wallet"
	protobufMain "develop/protobuf/main"
	"develop/main"
)

func testBalanceAndBonus(client protobufWallet.WalletClient) {
	Describe("GetBonus()", func() {
		Context("Корректный запрос", func() {
			It("Должен вернуть бонусную транзакцию", func() {
				dbWallet := createWallet(testWalletUserId)
				dbTransaction := createTransaction(dbWallet.ID)
				dbTransaction.Target = protobufWallet.TransactionTargetBonus
				dbTransaction.Save()

				defer deleteWallet(dbWallet.Id)

				protoTransaction, err := client.GetBonus(main.Ctx(), &protobufMain.ById{
					Id: dbWallet.Id,
				})

				Ω(err).Should(BeNil())

				compareTransaction(protoTransaction, dbTransaction, dbWallet)
			})
		})
	})

	Describe("GetTotalByUser()", func() {
		Context("проверка тоталов", func() {
			It("Должен вернуть успех без ошибок", func() {
				wallet := createWallet(testWalletUserId)
				defer deleteWallet(wallet.Id)
				total, _ := client.GetTotalByUser(main.Ctx(), &protobufMain.ByUser{
					User: wallet.UserId,
				})
				main.Logger().Infof("%v", total)
			})
		})
	})
}
