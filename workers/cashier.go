package workers

import (
	"errors"
	"fmt"
	"github.com/jackc/pgx/pgtype"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"net"
	"strconv"
	"time"

	protobufWallet "gitlab.com/sylogex/walleter/develop/protobuf/wallet"
	protobufMain "gitlab.com/sylogex/walleter/develop/protobuf/main"

	"gitlab.com/sylogex/walleter/conf"
	"gitlab.com/sylogex/walleter/queue"
	"gitlab.com/sylogex/walleter/models/db"
	"gitlab.com/sylogex/walleter/models/validator"
)

type Walleter struct {
	logger    *logrus.Entry
	publisher queue.Publisher
}

func (o *Walleter) Start(conf *conf.Config) error {
	var err error
	o.logger = main.Logger().WithField("worker", "operator")
	o.publisher, err = queue.CreatePublisher()
	if err != nil {
		return errors.New(fmt.Sprintf("failed to create publisher: %v", err))
	}

	addr := net.JoinHostPort(conf.Server.Host, conf.Server.Port)
	lis, err := net.Listen("tcp", addr)
	if err != nil {
		return errors.New(fmt.Sprintf("failed to listen: %v", err))
	}
	o.logger.Debugf("server listening %s", addr)

	grpcServer := grpc.NewServer(server.Options...)
	protobufWallet.RegisterWalleterServer(grpcServer, o)

	if err = grpcServer.Serve(lis); err != nil {
		return errors.New(fmt.Sprintf("failed to listen, err - %s", err))
	}
	return nil
}

func (o *Walleter) CreateWallet(ctx context.Context, in *protobufWallet.WalletRequest) (*protobufWallet.Wallet, error) {
	wallet := new(db.Wallet)
	if err := validator.Check(in); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if err := wallet.FindByUserIdAndType(in.User, in.Type); err == nil {
		return nil, status.Error(codes.AlreadyExists, "wallet with user_id and type already exist!")
	}

	if wallet.Type == protobufWallet.WalletTypeDemo {
		wallet.Balance = 1000
	}

	wallet.Status = protobufWallet.WalletStatusActive
	wallet.Locked = &pgtype.Timestamp{Status: pgtype.Null}
	wallet.Created = &pgtype.Timestamp{Time: time.Now().UTC(), Status: pgtype.Present}
	wallet.Updated = &pgtype.Timestamp{Time: time.Now().UTC(), Status: pgtype.Present}

	if err := wallet.Save(); err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	o.logger.Debugf("CreateWallet in postgres for user '%d' and %s success!", in.User, in.Currency)

	return wallet.ToProto(), nil
}

func (o *Walleter) GetWallet(ctx context.Context, in *protobufMain.ById) (*protobufWallet.Wallet, error) {
	wallet := new(db.Wallet)
	if err := validator.Check(in); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if err := wallet.FindById(wallet.Id); err != nil {
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return wallet.ToProto(), nil
}

func (o *Walleter) GetWalletByUserAndType(ctx context.Context, in *protobufWallet.ByUserAndWalletType) (*protobufWallet.Wallet, error) {
	wallet := new(db.Wallet)
	if err := validator.Check(in); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if err := wallet.FindByUserIdAndType(in.User, in.Type); err != nil {
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return wallet.ToProto(), nil
}

func (o *Walleter) GetWalletsByUser(ctx context.Context, in *protobufMain.ByUser) (*protobufWallet.WalletsReply, error) {
	var user int64
	if err := validator.Check(in); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	wallets := db.Wallets{}

	err = wallets.FindByUserId(user)
	if err != nil {
		return nil, status.Error(codes.NotFound, err.Error())
	}

	walletsReply := &protobufWallet.WalletsReply{
		Wallet: wallets.ToProto(),
	}

	return walletsReply, nil
}

func (o *Walleter) GetRequest(ctx context.Context, in *protobufMain.ById) (*protobufWallet.Request, error) {
	request := new(db.Request)
	if err := validator.Check(in); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	if err := request.FindById(request.Id); err != nil {
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return request.ToProtoWithBills(), nil
}

func (o *Walleter) DepositRequest(ctx context.Context, in *protobufWallet.Request) (*protobufWallet.Bill, error) {
	request := new(db.Request)
	if err := validator.Check(in); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	request.Type = protobufWallet.RequestTypeDeposit
	request.Status = protobufWallet.RequestStatusCreated
	request.Created = time.Now().UTC()
	request.Updated = time.Now().UTC()

	bill := &db.Bill{
		Type:    protobufWallet.BillTypeDeposit,
		Status:  protobufWallet.BillStatusConducted,
		Amount:  request.Amount,
		Gateway: request.Gateway,
		Created: time.Now().UTC(),
		Updated: time.Now().UTC(),
	}


	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	if err := request.Save(); err != nil {
		tx.Rollback()
		return nil, status.Error(codes.Internal, err.Error())
	}
	bill.RequestId = request.Id
	if err := bill.Save(); err != nil {
		tx.Rollback()
		return nil, status.Error(codes.Internal, err.Error())
	}
	if err := tx.Commit(); err != nil {
		traqnsa.Rollback()
		return nil, status.Error(codes.Internal, err.Error())
	}
	return bill.ToProtoWithRequestProto(), nil
}

func (o *Walleter) AcceptDeposit(ctx context.Context, in *protobufWallet.Bill) (*protobufWallet.Request, error) {
	bill := new(db.Bill)
	if err := validator.Check(in); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if err := bill.FindById(bill.Id); err != nil {
		return nil, status.Error(codes.NotFound, err.Error())
	}

	request, err := bill.GetRequest()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	wallet, err := request.GetWallet()
	if err != nil {
		return nil, status.Error(codes.NotFound, err.Error())
	}

	request.Status = protobufWallet.RequestStatusAccepted
	request.Updated = &pgtype.Timestamp{Time: time.Now().UTC(), Status: pgtype.Present}
	bill.Status = protobufWallet.BillStatusAccepted
	bill.Updated = &pgtype.Timestamp{Time: time.Now().UTC(), Status: pgtype.Present}

	transactionScore := &db.Transaction{
		WalletId:   request.WalletId,
		Type:        protobufWallet.TransactionTypeDeposit,
		Target:      protobufWallet.TransactionTargetBalance,
		Direction:   protobufWallet.TransactionDirectionIncrease,
		Reference:   protobufWallet.TransactionReferenceBill,
		ReferenceId: strconv.FormatInt(bill.Id, 10),
		Amount:      request.Amount,
		Created:     &pgtype.Timestamp{Time: time.Now().UTC(), Status: pgtype.Present},
	}

	balanceBefore := wallet.Balance

	transaction, err := tr.Begin(request, bill)
	if err != nil {
		return nil, status.Error(codes.Internal, "tx.Begin "+err.Error())
	}

	if err := request.Save(); err != nil {
		tx.Rollback()
		return nil, status.Error(codes.Internal, "request.Save "+err.Error())
	}

	err = wallet.UpdateBalance(request.Amount)
	if err != nil {
		tx.Rollback()
		return nil, status.Error(codes.Internal, "wallet.UpdateBalance "+err.Error())
	}

	transactionScore.Before = balanceBefore
	transactionScore.After = balanceBefore + request.Amount

	if err := transactionScore.Save(); err != nil {
		tx.Rollback()
		return nil, status.Error(codes.Internal, "tx.Save "+err.Error())
	}
	if err := bill.Save(); err != nil {
		tx.Rollback()
		return nil, status.Error(codes.Internal, "bill.Save "+err.Error())
	}

	if err := tx.Commit(); err != nil {
		tx.Rollback()
		return nil, status.Error(codes.Internal, "tx.Commit "+err.Error())
	}

	return request.ToProtoWithBills(), nil
}

// todo нужна ли проверка текущего статуса request'а и bill'а ? И возвращать может всё же Bill(с вложеннным Request и Wallet)
func (o *Walleter) DeclineDeposit(ctx context.Context, in *protobufWallet.Bill) (*protobufWallet.Request, error) {
	bill := new(db.Bill)
	if err := validator.Check(in); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if err := bill.FindById(bill.Id); err != nil {
		return nil, status.Error(codes.NotFound, err.Error())
	}
	request, err := bill.GetRequest()
	if err := request.FindById(bill.RequestId); err != nil {
		return nil, status.Error(codes.NotFound, err.Error())
	}

	request.Status = protobufWallet.RequestStatusDeclined
	request.Updated = &pgtype.Timestamp{Time: time.Now().UTC(), Status: pgtype.Present}
	bill.Status = protobufWallet.BillStatusDeclined
	bill.Updated = &pgtype.Timestamp{Time: time.Now().UTC(), Status: pgtype.Present}

	transaction, err := tr.Begin(request, bill)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	if err := request.Save(); err != nil {
		tx.Rollback()
		return nil, status.Error(codes.Internal, err.Error())
	}
	if err := bill.Save(); err != nil {
		tx.Rollback()
		return nil, status.Error(codes.Internal, err.Error())
	}
	if err := tx.Commit(); err != nil {
		tx.Rollback()
		return nil, status.Error(codes.Internal, err.Error())
	}

	return request.ToProtoWithBills(), nil
}

func (o *Walleter) CountDepositRequestsByWallet(ctx context.Context, in *protobufMain.ById) (*protobufMain.Count, error) {
	var walletId int64
	if err := validator.Check(in); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	requests := new(db.Requests)
	count, err := requests.CountByWalletIdTypeAndStatus(walletId, protobufWallet.RequestTypeDeposit, protobufWallet.RequestStatusAccepted)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	return &protobufMain.Count{Count: count.Count}, nil
}

func (o *Walleter) GetBill(ctx context.Context, in *protobufMain.ById) (*protobufWallet.Bill, error) {
	bill := new(db.Bill)
	if err := validator.Check(in); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if err := bill.FindById(bill.Id); err != nil {
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return bill.ToProtoWithRequestProto(), nil
}

func (o *Walleter) GetBillByGatewayAndReferenceId(ctx context.Context, in *protobufWallet.ByGatewayAndReferenceId) (*protobufWallet.Bill, error) {
	bill := new(db.Bill)
	if err := validator.Check(in); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if err := bill.FindByGatewayAndReferenceId(bill.Gateway, bill.ReferenceId.String); err != nil {
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return bill.ToProtoWithRequestProto(), nil
}

func (o *Walleter) SaveBill(ctx context.Context, in *protobufWallet.Bill) (*protobufWallet.Bill, error) {
	if err := validator.Check(in); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	bill := new(db.Bill)
	if err := bill.FindById(billId); err != nil {
		return nil, status.Error(codes.NotFound, err.Error())
	}

	bill.Type = in.billType
	bill.Status = in.billStatus
	bill.Gateway = in.billGateway
	bill.ReferenceId = in.billReferenceId
	bill.Problem = in.billProblem
	bill.WithdrawId = in.billWithdrawId
	bill.Mask = in.billMask
	bill.Country = in.billCountry
	bill.ExchangeRate = in.billExchangeRate
	bill.Updated = &pgtype.Timestamp{Time: time.Now().UTC(), Status: pgtype.Present}

	if err := bill.Save(); err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return bill.ToProtoWithRequestProto(), nil
}

func (o *Walleter) WithdrawRequest(ctx context.Context, in *protobufWallet.Request) (*protobufWallet.Request, error) {
	request := new(db.Request)
	if err := validator.Check(in); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	request.Type = protobufWallet.RequestTypeWithdraw
	request.Status = protobufWallet.RequestStatusCreated

	request.Created = time.Now().UTC()
	request.Updated = time.Now().UTC()

	if err := request.Save(); err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	requestProto := request.ToProto()

	return requestProto, nil
}

func (o *Walleter) ConductRequest(ctx context.Context, in *protobufMain.ById) (*protobufWallet.Request, error) {
	request := new(db.Request)
	if err := validator.Check(in); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if err := request.FindById(request.Id); err != nil {
		return nil, status.Error(codes.NotFound, err.Error())
	}

	wallet, err := request.GetWallet()
	if err := wallet.FindById(request.WalletId); err != nil {
		return nil, status.Error(codes.NotFound, err.Error())
	}
	balanceBefore := wallet.Balance

	request.Status = protobufWallet.RequestStatusConducted
	request.Updated = &pgtype.Timestamp{Time: time.Now().UTC(), Status: pgtype.Present}

	transactionScore := &db.Transaction{
		WalletId:   request.WalletId,
		Type:        protobufWallet.TransactionTypeWithdraw,
		Target:      protobufWallet.TransactionTargetBalance,
		Direction:   protobufWallet.TransactionDirectionDecrease,
		Reference:   protobufWallet.TransactionReferenceRequest,
		ReferenceId: strconv.FormatInt(request.Id, 10),
		Amount:      request.Amount,
		Created:     &pgtype.Timestamp{Time: time.Now().UTC(), Status: pgtype.Present},
	}

	tx, err := TX.Begin(wallet, request,, transactionScore)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	if err := wallet.UpdateBalance(-request.Amount); err != nil {
		tx.Rollback()
		return nil, status.Error(codes.Internal, err.Error())
	}

	transactionScore.Before = balanceBefore
	transactionScore.After = balanceBefore - request.Amount

	if err := request.Save(); err != nil {
		tx.Rollback()
		return nil, status.Error(codes.Internal, err.Error())
	}

	if err = transactionScore.Save(); err != nil {
		tx.Rollback()
		return nil, status.Error(codes.Internal, err.Error())
	}

	if err := tx.Commit(); err != nil {
		tx.Rollback()
		return nil, status.Error(codes.Internal, err.Error())
	}

	return request.ToProtoWithBills(), nil
}

func (o *Walleter) ProgressWithdraw(ctx context.Context, in *protobufWallet.Bill) (*protobufMain.Nil, error) {
	dbBill := new(db.Bill)
	if err := validator.Check(in); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	dbBill.Type = protobufWallet.BillTypeWithdraw
	dbBill.Status = protobufWallet.BillStatusProgressed
	dbBill.Created = time.Now().UTC()
	dbBill.Updated = time.Now().UTC()

	if err := dbBill.Save(); err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	body, _ := dbBill.ToProtoWithRequestProto().Marshal()
	o.publisher.Publish(queue.EventKey(queue.BillWithdrawKey), amqp.Publishing{
		Body: body,
	})

	return new(protobufMain.Nil), nil
}

func (o *Walleter) AcceptWithdraw(ctx context.Context, in *protobufWallet.Bill) (*protobufMain.Nil, error) {
	dbBill := new(db.Bill)
	if err := validator.Check(in); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	dbBill.Type = protobufWallet.BillTypeWithdraw
	dbBill.Status = protobufWallet.BillStatusAccepted
	dbBill.Created = &pgtype.Timestamp{Time: time.Now().UTC(), Status: pgtype.Present}
	dbBill.Updated = &pgtype.Timestamp{Time: time.Now().UTC(), Status: pgtype.Present}

	dbRequest := new(db.Request)
	if err := dbRequest.FindById(dbBill.RequestId); err != nil {
		return nil, status.Error(codes.NotFound, err.Error())
	}
	dbRequest.Status = protobufWallet.RequestStatusAccepted
	dbRequest.Updated = &pgtype.Timestamp{Time: time.Now().UTC(), Status: pgtype.Present}

	dbTransactionHold := &db.Transaction{
		WalletId: dbRequest.WalletId,
		Type:      protobufWallet.TransactionTypeWithdraw,
		Target:    protobufWallet.TransactionTargetHold,
		Direction: protobufWallet.TransactionDirectionDecrease,
		Reference: protobufWallet.TransactionReferenceBill,
		Before:    dbBill.Amount,
		Amount:    dbBill.Amount,
		After:     0,
		Created:   &pgtype.Timestamp{Time: time.Now().UTC(), Status: pgtype.Present},
	}

	tx, err := TX.Begin(dbRequest, dbBill, dbTransactionHold)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	if err = dbRequest.Save(); err != nil {
		tx.Rollback()
		return nil, status.Error(codes.Internal, err.Error())
	}

	if err = dbBill.Save(); err != nil {
		tx.Rollback()
		return nil, status.Error(codes.Internal, err.Error())
	}

	dbTransactionHold.ReferenceId = strconv.FormatInt(dbBill.Id, 10)

	if err = dbTransactionHold.Save(); err != nil {
		tx.Rollback()
		return nil, status.Error(codes.Internal, err.Error())
	}

	if err := tx.Commit(); err != nil {
		tx.Rollback()
		return nil, status.Error(codes.Internal, err.Error())
	}

	return new(protobufMain.Nil), nil
}

func (o *Walleter) DeclineWithdraw(ctx context.Context, in *protobufWallet.Bill) (*protobufMain.Nil, error) {
	bill := new(db.Bill)
	if err := validator.Check(in); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	bill.Type = protobufWallet.BillTypeWithdraw
	bill.Status = protobufWallet.BillStatusDeclined
	bill.Created = &pgtype.Timestamp{Time: time.Now().UTC(), Status: pgtype.Present}
	bill.Updated = &pgtype.Timestamp{Time: time.Now().UTC(), Status: pgtype.Present}

	request := new(db.Request)
	request.FindById(bill.RequestId)
	request.Status = protobufWallet.RequestStatusDeclined
	request.Updated = &pgtype.Timestamp{Time: time.Now().UTC(), Status: pgtype.Present}

	tx, err := TX.Begin(request, bill)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	if err = request.Save(); err != nil {
		tx.Rollback()
		return nil, status.Error(codes.Internal, err.Error())
	}

	if err = bill.Save(); err != nil {
		tx.Rollback()
		return nil, status.Error(codes.Internal, err.Error())
	}

	if err := tx.Commit(); err != nil {
		tx.Rollback()
		return nil, status.Error(codes.Internal, err.Error())
	}

	return nil, nil
}

func (o *Walleter) ProgressRefund(ctx context.Context, in *protobufWallet.Bill) (*protobufMain.Nil, error) {
	dbBill := new(db.Bill)
	if err := validator.Check(in); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	// todo может проверять request status?

	dbTransactionHoldLast := new(db.Transaction)
	if err := dbTransactionHoldLast.FindLastByTargetAndReference(protobufWallet.TransactionTargetHold,
		protobufWallet.TransactionReferenceRequest, dbBill.RequestId); err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	if dbBill.Amount > dbTransactionHoldLast.After {
		return nil, status.Error(codes.Canceled, "the refund amount is more than hold!")
	}

	dbBill.Type = protobufWallet.BillTypeRefund
	dbBill.Status = protobufWallet.BillStatusProgressed
	dbBill.Created = &pgtype.Timestamp{Time: time.Now().UTC(), Status: pgtype.Present}
	dbBill.Updated = &pgtype.Timestamp{Time: time.Now().UTC(), Status: pgtype.Present}

	if err := dbBill.Save(); err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	body, _ := dbBill.ToProtoWithRequestProto().Marshal()
	o.publisher.Publish(queue.EventKey(queue.BillRefundKey), amqp.Publishing{
		Body: body,
	})

	return new(protobufMain.Nil), nil
}

func (o *Walleter) AcceptRefund(ctx context.Context, in *protobufWallet.Bill) (*protobufMain.Nil, error) {
	dbBill := new(db.Bill)
	if err := validator.Check(in); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if err := dbBill.FindById(dbBill.Id); err != nil {
		return nil, status.Error(codes.NotFound, err.Error())
	}

	dbRequest, err := dbBill.GetRequest()
	if err := dbRequest.FindById(dbBill.RequestId); err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	dbTransactionLastHold := new(db.Transaction)
	if err := dbTransactionLastHold.FindLastByTargetAndReference(protobufWallet.TransactionTargetHold,
		protobufWallet.TransactionReferenceRequest, dbRequest.Id); err != nil {
		return nil, status.Error(codes.NotFound, err.Error())
	}

	if dbBill.Amount > dbTransactionLastHold.After {
		return nil, status.Error(codes.Canceled, "inaccessible refund amount")
	}

	dbBill.Status = protobufWallet.BillStatusAccepted
	dbBill.Updated = &pgtype.Timestamp{Time: time.Now().UTC(), Status: pgtype.Present}

	dbTransactionHold := new(db.Transaction)
	dbTransactionHold.WalletId = dbRequest.WalletId
	dbTransactionHold.Type = protobufWallet.TransactionTypeRefund
	dbTransactionHold.Target = protobufWallet.TransactionTargetHold
	dbTransactionHold.Direction = protobufWallet.TransactionDirectionDecrease
	dbTransactionHold.Reference = protobufWallet.TransactionReferenceBill
	dbTransactionHold.ReferenceId = strconv.FormatInt(dbBill.Id, 10)
	dbTransactionHold.Before = dbTransactionLastHold.After
	dbTransactionHold.Amount = dbBill.Amount
	dbTransactionHold.After = dbTransactionLastHold.After - dbBill.Amount
	dbTransactionHold.Created = &pgtype.Timestamp{Time: time.Now().UTC(), Status: pgtype.Present}

	tx, err := TX.Begin(dbRequest, dbBill, dbTransactionHold)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	if dbTransactionLastHold.After == dbBill.Amount {
		dbRequest.Status = protobufWallet.RequestStatusAccepted
		dbRequest.Updated = &pgtype.Timestamp{Time: time.Now().UTC(), Status: pgtype.Present}
		if err = dbRequest.Save(); err != nil {
			tx.Rollback()
			return nil, status.Error(codes.Internal, err.Error())
		}
	}

	if err = dbBill.Save(); err != nil {
		tx.Rollback()
		return nil, status.Error(codes.Internal, err.Error())
	}

	if err = dbTransactionHold.Save(); err != nil {
		tx.Rollback()
		return nil, status.Error(codes.Internal, err.Error())
	}

	if err := tx.Commit(); err != nil {
		tx.Rollback()
		return nil, status.Error(codes.Internal, err.Error())
	}

	return new(protobufMain.Nil), nil
}

func (o *Walleter) DeclineRefund(ctx context.Context, in *protobufWallet.Bill) (*protobufMain.Nil, error) {
	var id int64
	if err := validator.Check(in); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	dbBill := new(db.Bill)
	if err := dbBill.FindById(id); err != nil {
		return nil, status.Error(codes.NotFound, err.Error())
	}

	dbBill.Status = protobufWallet.BillStatusDeclined
	dbBill.Updated = &pgtype.Timestamp{Time: time.Now().UTC(), Status: pgtype.Present}
	if err := dbBill.Save(); err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return new(protobufMain.Nil), nil
}


func (o *Walleter) GetTotalByUser(ctx context.Context, msg *protobufMain.ByUser) (*protobufWallet.WalletTotal, error) {
	wallet := new(db.Wallet)
	if err := validator.Check(in); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	err := wallet.FindByUserIdAndType(msg.User, protobufWallet.WalletTypeReal)
	if err != nil {
		return nil, status.Error(codes.NotFound, err.Error())
	}
	total := wallet.GetTotal()
	walletTotal := &protobufWallet.WalletTotal{
		Deposit:  total.Deposit,
		Withdraw: total.Withdraw,
	}
	return walletTotal, nil
}
