package workers_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	protobufWallet "develop/protobuf/wallet"
	protobufMain "develop/protobuf/main"
	"develop/wallet/models/db"
	"develop/main"
	"github.com/jackc/pgx/pgtype"
)

func testBill(client protobufWallet.WalleterClient) {
	Describe("GetBill()", func() {
		Context("correct request", func() {
			It("should return correct bill from storage", func() {
				dbWallet := createWallet(testWalletUserId)
				dbRequest := createRequest(dbWallet.Id)
				createBill(dbRequest.Id)
				createBill(dbRequest.Id)
				dbBill := createBill(dbRequest.Id)

				defer deleteWallet(dbWallet.Id)

				protoBill, err := client.GetBill(main.Ctx(), &protobufMain.ById{
					Id: dbBill.Id,
				})
				Ω(err).Should(BeNil())

				compareBill(protoBill, dbBill, dbRequest, dbWallet, true)

				dbBillFromDb := new(db.Bill)
				Ω(dbBillFromDb.FindById(dbBill.Id)).Should(BeNil())

				compareBill(dbBillFromDb.ToProtoWithRequestProto(), dbBill, dbRequest, dbWallet, true)
			})
		})
	})

	Describe("SaveBill()", func() {
		Context("correct request", func() {
			It("save all fields in bill", func() {
				dbWallet := createWallet(testWalletUserId)
				dbRequest := createRequest(dbWallet.Id)
				dbBill := createBill(dbRequest.Id)

				defer deleteWallet(dbWallet.Id)

				originalValuesDbBill := &db.Bill{
					Id:          dbBill.Id,
					Type:        protobufWallet.BillTypeDeposit,
					Status:      protobufWallet.BillStatusProgressed,
					Gateway:     protobufWallet.GatewayChronoPay,
					ReferenceId: &pgtype.Varchar{String: "newRandom", Status: pgtype.Present},
					Problem:     protobufWallet.BillProblemPickUpCard,
					Mask:        &pgtype.Varchar{String: "newMask", Status: pgtype.Present},
					Country:     protobufMain.CountryNO,
				}

				protoBill, err := client.SaveBill(main.Ctx(), &protobufWallet.Bill{
					Id:          originalValuesDbBill.Id,
					Type:        originalValuesDbBill.Type,
					Status:      originalValuesDbBill.Status,
					Gateway:     originalValuesDbBill.Gateway,
					ReferenceId: &protobufMain.String{Value: originalValuesDbBill.ReferenceId.String},
					Problem:     originalValuesDbBill.Problem,
					Mask:        &protobufMain.String{Value: originalValuesDbBill.Mask.String},
					Country:     originalValuesDbBill.Country,
				})
				Ω(err).Should(BeNil())

				actualValuesDbBill := new(db.Bill)
				actualValuesDbBill.FindById(dbBill.Id)

				originalValuesDbBill.Created = actualValuesDbBill.Created
				originalValuesDbBill.Updated = actualValuesDbBill.Updated

				compareBill(protoBill, originalValuesDbBill, dbRequest, dbWallet, true)

				compareBill(protoBill, actualValuesDbBill, dbRequest, dbWallet, true)

			})
		})
	})

	Describe("GetBillByGatewayAndReferenceId()", func() {
		Context("correct request", func() {
			It("should select bill ByGatewayAndReferenceId and return it", func() {
				dbWallet := createWallet(testWalletUserId)
				dbRequest := createRequest(dbWallet.Id)
				createBill(dbRequest.Id)
				createBill(dbRequest.Id)
				createBill(dbRequest.Id)
				dbBill := createBill(dbRequest.Id)
				dbBill.Gateway = protobufWallet.Gateway(randIntMapKey(protobufWallet.Gateway_name))
				dbBill.ReferenceId = &pgtype.Varchar{String: randStringRunes(255), Status: pgtype.Present}
				dbBill.Save()

				defer deleteWallet(dbWallet.Id)

				protoBill, err := client.GetBillByGatewayAndReferenceId(main.Ctx(), &protobufWallet.ByGatewayAndReferenceId{
					Gateway: dbBill.Gateway,
					ReferenceId: dbBill.ReferenceId.String,
				})
				Ω(err).Should(BeNil())

				compareBill(protoBill, dbBill, dbRequest, dbWallet, true)
			})
		})
	})

	Describe("GetBillLastAcceptedDeposit() ", func() {
		Context("correct request ByWallet", func() {
			It("should return bill last accepted ByWalletMaskAndGateway", func() {
				dbWallet := createWallet(testWalletUserId)
				dbRequest := createRequest(dbWallet.Id)
				createBill(dbRequest.Id)
				createBill(dbRequest.Id)
				createBill(dbRequest.Id)
				dbBill := createBill(dbRequest.Id)
				dbBill.Status = protobufWallet.BillStatusAccepted
				dbBill.Type = protobufWallet.BillTypeDeposit
				dbBill.Save()

				defer deleteWallet(dbWallet.Id)

				protoBill, err := client.GetBillLastAcceptedDeposit(main.Ctx(), &protobufWallet.ByWalletMaskAndGateway{
					Wallet: dbWallet.Id,
				})
				Ω(err).Should(BeNil())

				compareBill(protoBill, dbBill, dbRequest, dbWallet, true)
			})
		})
		Context("correct request ByWalletMascAndGateway", func() {
			It("should return bill last accepted ByWalletMaskAndGateway", func() {
				dbWallet := createWallet(testWalletUserId)
				dbRequest := createRequest(dbWallet.Id)
				createBill(dbRequest.Id)
				createBill(dbRequest.Id)
				createBill(dbRequest.Id)
				dbBill := createBill(dbRequest.Id)
				dbBill.Status = protobufWallet.BillStatusAccepted
				dbBill.Mask = &pgtype.Varchar{String: randStringRunes(255), Status: pgtype.Present}
				dbBill.Gateway = protobufWallet.Gateway(randIntMapKey(protobufWallet.Gateway_name))
				dbBill.Type = protobufWallet.BillTypeDeposit
				dbBill.Save()

				defer deleteWallet(dbWallet.Id)

				protoBill, err := client.GetBillLastAcceptedDeposit(main.Ctx(), &protobufWallet.ByWalletMaskAndGateway{
					Wallet: dbWallet.Id,
					Mask: &protobufMain.String{Value: dbBill.Mask.String},
					Gateway: dbBill.Gateway,
				})
				Ω(err).Should(BeNil())

				compareBill(protoBill, dbBill, dbRequest, dbWallet, true)
			})
		})
		Context("incorrect request with wrong id", func() {
			It("should return bill last accepted ByWalletMaskAndGateway", func() {
				dbWallet := createWallet(testWalletUserId)
				dbRequest := createRequest(dbWallet.Id)
				createBill(dbRequest.Id)
				createBill(dbRequest.Id)
				createBill(dbRequest.Id)
				dbBill := createBill(dbRequest.Id)
				dbBill.Status = protobufWallet.BillStatusAccepted
				dbBill.Mask = &pgtype.Varchar{String: randStringRunes(255), Status: pgtype.Present}
				dbBill.Gateway = protobufWallet.Gateway(randIntMapKey(protobufWallet.Gateway_name))
				dbBill.Type = protobufWallet.BillTypeDeposit
				dbBill.Save()

				defer deleteWallet(dbWallet.Id)

				_, err := client.GetBillLastAcceptedDeposit(main.Ctx(), &protobufWallet.ByWalletMaskAndGateway{
					Wallet: dbWallet.Id - 1,
					Mask: &protobufMain.String{Value: dbBill.Mask.String},
					Gateway: dbBill.Gateway,
				})
				Ω(err).ShouldNot(BeNil())

			})
		})
	})
}
