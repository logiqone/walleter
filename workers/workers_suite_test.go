package workers_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"testing"
	"github.com/sirupsen/logrus"
	"github.com/jackc/pgx"
	"os"
	"github.com/streadway/amqp"
	"develop/main"
	
	"develop/wallet/models/db"

	"fmt"
	"github.com/jackc/pgx/pgtype"
	"time"
	protobufWallet "develop/protobuf/wallet"
	"math/rand"
	"lab.binatex.com/platform/helpers/app"
)

func TestWorkers(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Workers Suite")
}

func InitApp(testName string, appcfg *app.Config) {

	//logrus.SetOutput(GinkgoWriter)
	logrus.SetLevel(logrus.DebugLevel)
	entry := logrus.WithFields(logrus.Fields{
		"app": testName,
	})

	dbPool, err := pgx.NewConnPool(
		pgx.ConnPoolConfig{
			ConnConfig: pgx.ConnConfig{
				Host:     appcfg.DB.Host,
				Port:     appcfg.DB.Port,
				Database: appcfg.DB.Name,
				User:     appcfg.DB.User,
				Password: appcfg.DB.Pass,
			},
		},
	)
	if err != nil {
		entry.Errorf("Cannot connect to database postgres!" )
		os.Exit(1)
	}

	amqpConn, err := amqp.Dial(fmt.Sprintf("amqp://%s:%s@%s:%d/%s", appcfg.Amqp.User, appcfg.Amqp.Password, appcfg.Amqp.Host, appcfg.Amqp.Port, appcfg.Amqp.Vhost))
	if err != nil {
		entry.Fatal(err)
	}

	if err != nil {
		entry.Fatalf("connection.open: %s", err)
	}
}

func createWallet(userId int64) *db.Wallet {
	dbWallet := &db.Wallet{
		UserId: userId,
		Created: &pgtype.Timestamp{Time: time.Now().UTC(), Status: pgtype.Present},
		Updated: &pgtype.Timestamp{Time: time.Now().UTC(), Status: pgtype.Present},
	}
	if err := dbWallet.Save(); err != nil {
		Fail("helper: dbWallet.Save() error : " + err.Error())
	}
	return dbWallet
}

func deleteWallet(walletId int64) {
	wallet := new(db.Wallet)
	wallet.FindById(walletId)
	wallet.Remove()
}

func deleteWalletsByUserId(userId int64) {
	dbWallets := &db.Wallets{}
	dbWallets.FindByUserId(userId)
	for _, dbWallet := range *dbWallets{
		dbWallet.Remove()
	}
}

func deleteWallets(protobufWallets []*protobufWallet.Wallet) {
	for _, protobufWallet := range protobufWallets {
		deleteWallet(protobufWallet.Id)
	}
}

func createRequest(walletId int64) *db.Request {
	dbRequest := &db.Request{
		WalletId: walletId,
		PurseId: &pgtype.Int8{Status: pgtype.Null},
		Bonus: &pgtype.Float8{Status: pgtype.Null},
		Turnover: &pgtype.Float8{Status: pgtype.Null},
		UseCoupon: &pgtype.Bool{Status: pgtype.Null},
		CouponId: &pgtype.Int8{Status: pgtype.Null},
		UseRate: &pgtype.Bool{Status: pgtype.Null},
		RateId: &pgtype.Int8{Status: pgtype.Null},
		GaCid: &pgtype.Varchar{Status: pgtype.Null},
		IpAddr: &pgtype.Varchar{Status: pgtype.Null},
		Created: &pgtype.Timestamp{Time: time.Now().UTC(), Status: pgtype.Present},
		Updated: &pgtype.Timestamp{Time: time.Now().UTC(), Status: pgtype.Present},
	}

	if err := dbRequest.Save(); err != nil {
		Fail("helper: dbRequest.Save() error: " + err.Error())
	}

	return dbRequest
}

func createBill(requestId int64) *db.Bill {
	dbBill := &db.Bill{
		RequestId: requestId,
		ReferenceId: &pgtype.Varchar{Status: pgtype.Null},
		WithdrawId: &pgtype.Int8{Status: pgtype.Null},
		Mask: &pgtype.Varchar{Status: pgtype.Null},
		ExchangeRate: &pgtype.Float8{Status: pgtype.Null},
		Created: &pgtype.Timestamp{Time: time.Now().UTC(), Status: pgtype.Present},
		Updated: &pgtype.Timestamp{Time: time.Now().UTC(), Status: pgtype.Present},
	}

	if err := dbBill.Save(); err != nil {
		Fail("helper: dbBill.Save() error: " + err.Error())
	}

	return dbBill
}

func createTransaction(walletId int64) *db.Transaction {
	dbTransaction := &db.Transaction{
		WalletId: walletId,
		Created: &pgtype.Timestamp{Time: time.Now().UTC(), Status: pgtype.Present},
	}

	if err := dbTransaction.Save(); err != nil {
		Fail("helper: dbTransaction.Save() error: " + err.Error())
	}
	return dbTransaction
}

func compareWallet(protobufWallet *protobufWallet.Wallet, dbWallet *db.Wallet) {
	Ω(protobufWallet.Id).To(Equal(dbWallet.Id))
	Ω(protobufWallet.User).To(Equal(dbWallet.UserId))
	Ω(protobufWallet.Currency).To(Equal(dbWallet.Currency))
	Ω(protobufWallet.Type).To(Equal(dbWallet.Type))
	Ω(protobufWallet.Status).To(Equal(dbWallet.Status))
	Ω(protobufWallet.Balance).To(Equal(dbWallet.Balance))
	Ω(protobufWallet.ProducedTurnover).To(Equal(dbWallet.ProducedTurnover))
	Ω(protobufWallet.RequiredTurnover).To(Equal(dbWallet.RequiredTurnover))
	if dbWallet.Locked != nil && dbWallet.Locked.Status == pgtype.Present{
		Ω(protobufWallet.Locked).To(Equal(dbWallet.Locked.Time.Format(time.RFC3339)))
	} else {
		Ω(protobufWallet.Locked).To(Equal(""))
	}
	Ω(protobufWallet.Created).To(Equal(dbWallet.Created.Time.Format(time.RFC3339)))
	Ω(protobufWallet.Updated).To(Equal(dbWallet.Updated.Time.Format(time.RFC3339)))
}

func compareRequest(protoRequest *protobufWallet.Request, dbRequest *db.Request, dbWallet *db.Wallet, dbBills []*db.Bill) {
	Ω(protoRequest.Id).To(Equal(dbRequest.Id))

	compareWallet(protoRequest.Wallet, dbWallet)

	Ω(protoRequest.Type).To(Equal(dbRequest.Type))
	Ω(protoRequest.Status).To(Equal(dbRequest.Status))
	Ω(protoRequest.Source).To(Equal(dbRequest.Source))
	Ω(protoRequest.Amount).To(Equal(dbRequest.Amount))
	if dbRequest.Bonus != nil && dbRequest.Bonus.Status == pgtype.Present {
		Ω(protoRequest.GetBonus().Value).To(Equal(dbRequest.Bonus.Float))
	} else {
		Ω(protoRequest.GetBonus()).Should(BeNil())
	}
	if dbRequest.Turnover != nil && dbRequest.Turnover.Status == pgtype.Present {
		Ω(protoRequest.GetTurnover().Value).To(Equal(dbRequest.Turnover.Float))
	} else {
		Ω(protoRequest.GetTurnover()).Should(BeNil())
	}
	if dbRequest.UseCoupon != nil && dbRequest.UseCoupon.Status == pgtype.Present {
		Ω(protoRequest.GetUseCoupon().Value).To(Equal(dbRequest.UseCoupon.Bool))
	} else {
		Ω(protoRequest.GetUseCoupon()).Should(BeNil())
	}
	if dbRequest.CouponId != nil && dbRequest.CouponId.Status == pgtype.Present {
		Ω(protoRequest.GetCouponId().Value).To(Equal(dbRequest.CouponId.Int))
	} else {
		Ω(protoRequest.GetCouponId()).Should(BeNil())
	}
	if dbRequest.UseRate != nil && dbRequest.UseRate.Status == pgtype.Present {
		Ω(protoRequest.GetUseRate().Value).To(Equal(dbRequest.UseRate.Bool))
	} else {
		Ω(protoRequest.GetUseRate()).Should(BeNil())
	}
	if dbRequest.RateId != nil && dbRequest.RateId.Status == pgtype.Present {
		Ω(protoRequest.GetRateId().Value).To(Equal(dbRequest.RateId.Int))
	} else {
		Ω(protoRequest.GetRateId()).Should(BeNil())
	}
	if dbRequest.GaCid != nil && dbRequest.GaCid.Status == pgtype.Present {
		Ω(protoRequest.GetGaCid().Value).To(Equal(dbRequest.GaCid.String))
	} else {
		Ω(protoRequest.GetGaCid()).Should(BeNil())
	}
	if dbRequest.IpAddr != nil && dbRequest.IpAddr.Status == pgtype.Present {
		Ω(protoRequest.GetIpAddr().Value).To(Equal(dbRequest.IpAddr.String))
	} else {
		Ω(protoRequest.GetIpAddr()).Should(BeNil())
	}
	Ω(protoRequest.Created).To(Equal(dbRequest.Created.Time.Format(time.RFC3339)))
	Ω(protoRequest.Updated).To(Equal(dbRequest.Updated.Time.Format(time.RFC3339)))

	Ω(len(protoRequest.Bills)).Should(Equal(len(dbBills)))

	for i, protoBill := range protoRequest.Bills {
		compareBill(protoBill, dbBills[i], dbRequest, dbWallet, false)
	}
}

func compareBill(protoBill *protobufWallet.Bill, dbBill *db.Bill, dbRequest *db.Request, dbWallet *db.Wallet, withRequest bool) {
	Ω(protoBill.GetId()).To(Equal(dbBill.Id))
	if withRequest {
		compareRequest(protoBill.Request, dbRequest, dbWallet, []*db.Bill{})
	} else {
		Ω(protoBill.Request.Id).To(Equal(dbBill.RequestId))
	}
	Ω(protoBill.GetType()).To(Equal(dbBill.Type))
	Ω(protoBill.GetStatus()).To(Equal(dbBill.Status))
	Ω(protoBill.GetAmount()).To(Equal(dbBill.Amount))
	Ω(protoBill.GetGateway()).To(Equal(dbBill.Gateway))
	if dbBill.ReferenceId != nil && dbBill.ReferenceId.Status == pgtype.Present {
		Ω(protoBill.GetReferenceId().Value).To(Equal(dbBill.ReferenceId.String))
	} else {
		Ω(protoBill.GetWithdrawId()).Should(BeNil())
	}
	Ω(protoBill.GetProblem()).To(Equal(dbBill.Problem))
	if dbBill.WithdrawId != nil && dbBill.WithdrawId.Status == pgtype.Present {
		Ω(protoBill.GetWithdrawId().Value).To(Equal(dbBill.WithdrawId.Int))
	} else {
		Ω(protoBill.GetWithdrawId()).Should(BeNil())
	}
	if dbBill.Mask != nil && dbBill.Mask.Status == pgtype.Present {
		Ω(protoBill.GetMask().Value).To(Equal(dbBill.Mask.String))
	} else {
		Ω(protoBill.GetMask()).Should(BeNil())
	}
	Ω(protoBill.GetCountry()).To(Equal(dbBill.Country))
	if dbBill.ExchangeRate != nil && dbBill.ExchangeRate.Status == pgtype.Present {
		Ω(protoBill.GetExchangeRate().Value).To(Equal(dbBill.ExchangeRate.Float))
	} else {
		Ω(protoBill.GetExchangeRate()).Should(BeNil())
	}
	Ω(protoBill.GetCreated()).To(Equal(dbBill.Created.Time.Format(time.RFC3339)))
	Ω(protoBill.GetUpdated()).To(Equal(dbBill.Updated.Time.Format(time.RFC3339)))
}

func compareTransaction(protoTransaction *protobufWallet.Transaction, dbTransaction *db.Transaction, dbWallet *db.Wallet) {
	Ω(protoTransaction.Id).To(Equal(dbTransaction.Id))

	Ω(protoTransaction.Wallet.Id).To(Equal(dbWallet.Id))
	Ω(protoTransaction.Wallet.User).To(Equal(dbWallet.UserId))
	Ω(protoTransaction.Wallet.Currency).To(Equal(dbWallet.Currency))
	Ω(protoTransaction.Wallet.Type).To(Equal(dbWallet.Type))
	Ω(protoTransaction.Wallet.Status).To(Equal(dbWallet.Status))
	Ω(protoTransaction.Wallet.Balance).To(Equal(dbWallet.Balance))
	Ω(protoTransaction.Wallet.ProducedTurnover).To(Equal(dbWallet.ProducedTurnover))
	Ω(protoTransaction.Wallet.RequiredTurnover).To(Equal(dbWallet.RequiredTurnover))

	Ω(protoTransaction.Type).To(Equal(dbTransaction.Type))
	Ω(protoTransaction.Target).To(Equal(dbTransaction.Target))
	Ω(protoTransaction.Direction).To(Equal(dbTransaction.Direction))
	Ω(protoTransaction.Reference).To(Equal(dbTransaction.Reference))
	Ω(protoTransaction.ReferenceId).To(Equal(dbTransaction.ReferenceId))
	Ω(protoTransaction.Before).To(Equal(dbTransaction.Before))
	Ω(protoTransaction.Amount).To(Equal(dbTransaction.Amount))
	Ω(protoTransaction.After).To(Equal(dbTransaction.After))
	Ω(protoTransaction.Created).To(Equal(dbTransaction.Created.Time.Format(time.RFC3339)))
}

func randIntMapKey(m map[int32]string) int32 {
	indexKey := make(map[int32]int32)
	var j int32 = 0
	for k := range m {
		if k != 0 {
			indexKey[j] = k
			j++
		}
	}

	i := rand.Intn(len(m) - 1)

	//main.Logger().Infof("indexKey: %v", indexKey)
	//main.Logger().Infof("i: %d", i)
	//main.Logger().Infof("result: %d", indexKey[int32(i)])

	return indexKey[int32(i)]
}

func randInt64() int64 {
	return rand.Int63n(1<<63 - 2) + 1
}

func randBool() bool {
	return rand.Float32() < 0.5
}

func randStringRunes(n int) string {
	letterRunes := []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

	rand.Seed(time.Now().UnixNano())
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}