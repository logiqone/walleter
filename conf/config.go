package conf

type Config struct {
	Grpc			*Grpc             `yaml:"grpc"`
	Logger    	  	*Logger           `yaml:"logger"`
	Amqp    		*Amqp             `yaml:"amqp"`
	Postrges      	*Postrges         `yaml:"postgres"`
}

type Grpc struct {
	Host string `yaml:"host"`
	Port string `yaml:"port"`
}

type Logger struct {
	Level string `yaml:"level"`
}

type Amqp struct {
	Host     string `yaml:"host"`
	Port     int    `yaml:"port"`
	Domain   string `yaml:"domain"`
	User     string `yaml:"user"`
	Password string `yaml:"pass"`
	Conn 	 int 	`yaml:"conn"`
}

type Postrges struct {
	Host string `yaml:"host"`
	Port uint16 `yaml:"port"`
	Name string `yaml:"name"`
	User string `yaml:"user"`
	Pass string `yaml:"pass"`
	Conn int    `yaml:"conn"`
}
