package db

import (
	protobufMain "develop/protobuf/main"
	protobufWallet "develop/protobuf/wallet"
	"time"
)

//go:generate reform

//reform:wallet
type Wallet struct {
	Id       int64                    `reform:"id"`
	UserId   int64                    `reform:"user_id"`
	Currency protobufMain.Currency     `reform:"currency"`
	Status   protobufWallet.WalletStatus `reform:"status"`
	Balance  float64                  `reform:"balance"`
	Locked   *time.Time               `reform:"locked"`
	Created  time.Time                `reform:"created"`
	Updated  time.Time                `reform:"updated"`
}
