package db

import (
	protobufWallet "develop/protobuf/wallet"
	"time"
)

//go:generate reform

//reform:transaction
type Transaction struct {
	Id          int64                            	`reform:"id"`
	WalletId    int64                            	`reform:"wallet_id"`
	Type        protobufWallet.TransactionType      `reform:"type"`
	Direction   protobufWallet.TransactionDirection `reform:"direction"`
	Reference   protobufWallet.TransactionReference `reform:"reference"`
	ReferenceId string                           	`reform:"reference_id"`
	Before      float64                          	`reform:"before"`
	Amount      float64                          	`reform:"amount"`
	After       float64                          	`reform:"after"`
	Created     *time.Time                       	`reform:"created"`
}
